package br.com.doctum.optativa.aulamobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        TextView textView = (TextView) findViewById(R.id.textViewDash);
        textView.setText("Eu sou lindo");
    }
}
