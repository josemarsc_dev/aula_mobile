package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.buttonEntrar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTextUser = (EditText) findViewById(R.id.campoUsuario);
                String user = editTextUser.getText().toString();

                EditText editTextSenha = (EditText) findViewById(R.id.campoSenha);
                String senha = editTextSenha.getText().toString();

                if(user.equals("josemar") && senha.equals("1234")) {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                } else {
                    Log.i("logado", "Login ou senha incorretos");
                    alert("Erro ao efetuar login");
                }
            }
        });

    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public Context getContext() {
        return this;
    }
}
